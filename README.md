# NRF24L01P #
## Overview ##
* A library for the Nordic Semiconductor's nRF24L01+ 2,4GHz radio module
* For Atmel AVR Microcontrollers (refactoring for other architectures should be relatively easy)
* Written in C

## Features (when fully complete) ##
* High performance
* Provides easy setup & data transfer functions
* Has usage examples
* Change used pins easily

# Current phase of the Project #
* Not actively worked on

## Done ##
* USART setup and output functions (for debugging without a HW debugger)
* SPI setup and R/W functions
* Byte R/W operations for the nRF
* Multi-byte R/W operations for the nRF
* Transmit control functions
* R/W status query functions
* Simple functions for RX + TX transfers
* Simple functions for the setup process
* Power on/shutdown functions
* Custom ACK payloads

## TODO ##
* Optimize timings
* Optimize performance
* Try HW interrupts
* More usage examples