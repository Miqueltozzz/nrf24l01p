/*
 * USART.c
 *
 * Created: 21.2.2017 17.52.30
 *  Author: Miqueltozzz
 *  Code based on http://extremeelectronics.co.in/avr-tutorials/using-the-usart-of-avr-microcontrollers-reading-and-writing-data/
 */ 
#define F_CPU 16000000
#include <avr/io.h>
#include "USART.h"
#include <string.h>	//For strlen() function

void USARTInit()
{
	//Set Baud rate 19200 @ 16MHz cpu
	uint16_t ubrr_value = 51;
   UBRR0L = ubrr_value;
   UBRR0H = (ubrr_value>>8);

   /*Set Frame Format
   >> Asynchronous mode
   >> No Parity
   >> 1 StopBit
   >> char size 8
   */
   UCSR0C=(1<<USBS0)|(3<<UCSZ00);
   //Enable The receiver and transmitter
   UCSR0B=(1<<RXEN0)|(1<<TXEN0);
}

//This function is used to read the available data
//from USART. This function will wait until data is
//available.
char USARTReadChar()
{
   //Wait until a data is available
   while(!(UCSR0A & (1<<RXC0)))
   {
      //Do nothing
   }
   //Now USART has got data from host
   //and is available is buffer
   return UDR0;
}

//This function writes the given "data" to
//the USART which then transmit it via TX line
void USARTWriteChar(char data)
{
   //Wait until the transmitter is ready
   while(!(UCSR0A & (1<<UDRE0)))
   {
      //Do nothing
   }
   //Now write the data to USART buffer
   UDR0=data;
}

void USARTWriteCharArray(char array[])
{
	for(int a=0; a<strlen(array); a++)
	{
		USARTWriteChar(array[a]);
	}
}

void USARTWriteUint8Array(uint8_t array[], int length)
{
	for(int a=0; a<length; a++)
	{
		USARTWriteUint8(array[a]);
	}
}

void USARTWriteUint8(uint8_t input)	//writes the input as a hex value
{
	USARTWriteCharArray("[0x");
	uint8_t temp = 0;
	uint8_t charValue = 0;
	temp = (input>>4);
	if (temp>9)	//convert values 10-15 to ASCII letters A-F
	{
		charValue = temp + 55;
	}
	else	//convert values 0-9 to ASCII numbers
	{
		charValue = temp + 48;
	}
	USARTWriteChar(charValue);
	temp = (input & 0x0F);
	if (temp>9)		//convert values 10-15 to ASCII letters A-F
	{
		charValue = temp + 55;
	}
	else	//convert values 0-9 to ASCII numbers
	{
		charValue = temp + 48;
	}
	USARTWriteChar(charValue);
	USARTWriteChar(']');
}