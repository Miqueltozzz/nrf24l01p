/*
 * NRF24L01P.h
 *
 * Created: 21.2.2017 16.48.06
 *  Author: Miqueltozzz
 */ 
#ifndef NRF24L01P_H_
#define NRF24L01P_H_

#include "nRF24L01_registers.h"

void _nrf_spi_init();
uint8_t _nrf_spi_write_byte(uint8_t cData);
uint8_t _nrf_spi_read_byte();
uint8_t _nrf_read_byte(uint8_t reg);
void _nrf_write_byte(uint8_t reg, uint8_t data);
void _nrf_write_data(uint8_t reg, uint8_t *start_ptr, uint8_t length);
void _nrf_read_data(uint8_t reg, uint8_t *start_ptr, uint8_t length);
void _nrf_pulse_ce();
void nrf_start_listening();
void nrf_stop_listening();
bool nrf_data_available();
bool nrf_transmit_success();
bool nrf_transmit_fail();
void nrf_write_tx_payload(uint8_t *start_ptr, uint8_t length);
void nrf_read_rx_payload(uint8_t *start_ptr, uint8_t length);
void nrf_set_transmit_power (uint8_t transmitPower);
void nrf_set_tx_address (uint8_t *address_ptr);
void nrf_set_address_width (uint8_t width);
void nrf_enable_auto_ack (uint8_t pipe, bool state);
void nrf_enable_data_pipe (uint8_t pipe, bool state);
void nrf_set_rx_address(uint8_t pipe, uint8_t *address_ptr);
void nrf_set_channel(uint8_t channel);
void nrf_set_data_rate (uint8_t dataRate);
void nrf_set_payload_length (uint8_t pipe, uint8_t length);
void nrf_set_retransmit_delay(uint16_t delay);
void nrf_set_retransmit_count(uint8_t count);
void nrf_power_up();
void nrf_power_down();
void nrf_set_crc_length(uint8_t length);
void nrf_enable_ack_payload(bool state);
void nrf_enable_dynamic_payload_length(uint8_t pipe, bool state);
void nrf_write_ack_payload(uint8_t pipe, uint8_t *data_ptr, uint8_t length);

#endif /* NRF24L01P_H_ */