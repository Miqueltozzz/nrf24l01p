/*
 * example.c
 *
 * Created: 21.2.2017 16.45.29
 * Author : Miqueltozzz
 * 
 * Description:
 * 
 * This is an example program made to show how to use the NRF24L01P library.
 * This program does the needed setup for the nRF, and then depending on the set role
 * starts transmitting or receiving data. The sent/received data is written on the USART.
 */ 
#define F_CPU 16000000UL	//This should be configured in project settings, but cant find where
#define ROLE_TX 0
#define ROLE_RX 1

#include <avr/io.h>		//IO definitions for the MCU set in project settings
#include <util/delay.h>
#include <stdbool.h>
#include "USART.h"
#include "NRF24L01P.h"

int main(void)
{
	const uint8_t role = ROLE_TX;
	uint8_t address[] = {0,1,2,3,4};	//Address, same can be used on both ends when no special features are needed
	USARTInit();
	_nrf_spi_init();
	
	//nRF setup
	_delay_ms(100);	//nRF startup time
	nrf_enable_auto_ack(0, true);	//Enable auto-ack for pipe 0
	nrf_enable_data_pipe(0, true);	//Enable data pipe 0
	nrf_set_address_width(5);	//Set 5 byte address width
	nrf_set_channel(1);	//Set RF channel 1, 2,401GHz
	nrf_set_transmit_power(3);	//Set transmit power, 0 dBm
	nrf_set_data_rate(0);	//Set 250 kbps data rate
	nrf_set_retransmit_delay(750);	//Set 750 us retransmission delay
	nrf_set_retransmit_count(15);	//15 transmission retries
	nrf_set_rx_address(0,address);	//Set RX data pipe 0 address
	nrf_set_tx_address(address);	//Set TX address
	nrf_set_payload_length(0, 5);	//Set 5 byte payload length for RX data pipe 0
	nrf_set_crc_length(2);	//Set 2 bytes CRC
	nrf_power_up();	//Go from powerDown state to standby
	_delay_ms(5);	//PowerDown->standby delay
	nrf_enable_ack_payload(true);
	nrf_enable_dynamic_payload_length(0, true);	//Enable dynamic payload length for data pipe 0
	USARTWriteCharArray("Initialization done\n");
	
    while (1)	//Main program loop
    {
		if(role == ROLE_TX)	//TX mode
		{
			//Setup for TX role
			uint8_t txData[5] = {7,1,2,3,0};	//Payload data
			uint8_t rxData[5] = {0,0,0,0,0};	//ACK payload will be put here
			_delay_ms(100);	//Startup time for the nrf
			while(1)	//TX program loop
			{
				nrf_write_tx_payload(txData, sizeof(txData));
				USARTWriteUint8Array(txData, sizeof(txData));
				USARTWriteChar(0x09);	//0x09 = tab
				while(1)
				{
					if(nrf_transmit_fail())
					{
						USARTWriteCharArray("Transmit failed\n");
						break;
					}
					else if(nrf_transmit_success())
					{
						USARTWriteCharArray("Transmit success\n");
						break;
					}
				}
				if(nrf_data_available())	//If received ACK payload
				{
					nrf_read_rx_payload(rxData, sizeof(rxData));
					USARTWriteUint8Array(rxData, sizeof(rxData));
					USARTWriteChar(0x09);	//0x09 = tab
					USARTWriteCharArray("Ack payload received\n");
				}
				_delay_ms(1000);	//Delay between transmits
				
				//Change first and last byte of payload between each transmit for debugging purposes
				txData[0]--;
				txData[4]++;
				if(txData[4]>7)
				{
					txData[4]=0;
					txData[0]=7;
				}
			}
		}	//TX role end	
		else //RX role
		{
			//Setup for RX role
			uint8_t rxData[5] = {0,0,0,0,0};	//RX Payload will be put here
			nrf_start_listening();
			while(1)	//RX Program loop
			{
				if(nrf_data_available())
				{
					nrf_write_ack_payload(0,rxData, 5);	//Write old received data as ACK payload
					nrf_read_rx_payload(rxData, sizeof(rxData));
					USARTWriteUint8Array(rxData, sizeof(rxData));
					USARTWriteChar(0x09);	//0x09 = tab
					USARTWriteCharArray("Data received\n");	
				}
			}
		}	//TX mode end
    }	//Main program loop end
}

