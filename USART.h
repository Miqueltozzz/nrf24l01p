/*
 * USART.h
 *
 * Created: 21.2.2017 17.52.20
 *  Author: Miqueltozzz
 *  Code derived from http://extremeelectronics.co.in/avr-tutorials/using-the-usart-of-avr-microcontrollers-reading-and-writing-data/
 */ 
#ifndef USART_H_
#define USART_H_

void USARTInit();
char USARTReadChar();
void USARTWriteChar(char data);
void USARTWriteCharArray(char array[]);
void USARTWriteUint8Array(uint8_t array[], int length);
void USARTWriteUint8(uint8_t input);

#endif /* USART_H_ */