/*
 * NRF24L01P.c
 *
 * Created: 21.2.2017 16.48.20
 * Author: Miqueltozzz
 */ 
#define F_CPU 16000000UL	//This should be defined in project settings, can't find where

//Pin setup
#define CSN_DDR DDRB
#define CSN_PORT PORTB
#define CSN_PIN PB0	//Digital pin 53 on Arduino Mega 2560, has to be output to prevent SPI going slave

#define CE_DDR DDRB
#define CE_PORT PORTB
#define CE_PIN PB5		//Digital pin 11 on Arduino Mega 2560

#define SPI_DDR DDRB
#define SPI_PORT PORTB
#define SCK_PIN PB1	//Digital pin 52 on Arduino Mega 2560
#define MOSI_PIN PB2	//Digital pin 51 on Arduino Mega 2560
#define MISO_PIN PB2	//Digital pin 53 on Arduino Mega 2560

#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "NRF24L01P.h"
//Register map is included in the header file

/*
 *Function: _nrf_spi_init()
 *
 *Initializes the SPI
 *Sets DDR for SPI, CSN and CE
 *SS pin has to be output or set high, or SPI goes to slave mode
 */
void _nrf_spi_init()
{
	//Set as outputs: SCK, MOSI, CSN, CE
	SPI_DDR |= (1<<SCK_PIN) | (1<<MOSI_PIN);
	CE_DDR |= (1<<CE_PIN);
	CSN_DDR |= (1<<CSN_PIN);
	//SPI: Enable, Master, clock rate is default clk/16
	SPCR |= (1<<SPE) | (1<<MSTR);
	CSN_PORT |= (1<<CSN_PIN);	//Set CSN high
	CE_PORT &= ~(1<<CE_PIN);	//Set CE low
}

/*
 *Function: _nrf_spi_write_byte(uint8_t data)
 *
 *Writes a byte to the SPI, and returns the received byte
 */
uint8_t _nrf_spi_write_byte(uint8_t data)
{
	//Load byte to send to data register
	SPDR = data;
	//wait for transmission to complete
	while(!(SPSR & (1<<SPIF)));
	//return data gotten from spi
	return SPDR;
}

/*
 *Function: _nrf_spi_read_byte()
 *
 *Sends a dummy byte to SPI, and returns the received byte.
 *_nrf_spi_write_byte could also be used for same functionality, but this should be slightly faster
 */
uint8_t _nrf_spi_read_byte()
{
	//Load dummy byte to data register
	SPDR = 0x00;
	//wait for transmission to complete
	while(!(SPSR & (1<<SPIF)));
	//return data gotten from spi
	return SPDR;
}

/*
 *Function: _nrf_read_byte(uint8_t reg)
 *
 *Reads the register reg from the nRF module using SPI
 *CSN to SCK setup delay is specified as only 2ns (datasheet page 53), no delays are needed
 */
uint8_t _nrf_read_byte(uint8_t reg)
{
	CSN_PORT &= ~(1<<CSN_PIN);	//Set CSN low
	_nrf_spi_write_byte(R_REGISTER + reg);	//Tell what register to read
	reg = _nrf_spi_read_byte();	//Put result to parameter variable to save a few clock cycles
	CSN_PORT |= (1<<CSN_PIN);	//Set CSN high
	return reg;
}

/*
 *Function: _nrf_write_byte(uint8_t reg, uint8_t data)
 *
 *Writes byte "data" to register "reg" on the NRF using SPI
 *CSN to SCK setup delay is specified as only 2ns (datasheet page 53), no delays are needed
 */
void _nrf_write_byte(uint8_t reg, uint8_t data)
{
	CSN_PORT &= ~(1<<CSN_PIN);	//Set CSN LOW
	_nrf_spi_write_byte(W_REGISTER + reg);	//Tell nRF where we will write
	_nrf_spi_write_byte(data);	//Send data to write
	CSN_PORT |= (1<<CSN_PIN);	//Set CSN high
}

/*
 *Function: _nrf_write_data(uint8_t reg, uint8_t *start_ptr, uint8_t length)
 *
 *writes multiple bytes to a specified register on the nRF.
 *Needed for example for writing addresses and payload data.
 */
void _nrf_write_data(uint8_t reg, uint8_t *start_ptr, uint8_t length)
{
	CSN_PORT &= ~(1<<CSN_PIN);	//Set CSN LOW
	_nrf_spi_write_byte(W_REGISTER + reg);	//Tell nRF where we will write
	for(int a=0; a<length; a++)
	{
		_nrf_spi_write_byte(*start_ptr);
		start_ptr++;
	}
	CSN_PORT |= (1<<CSN_PIN);	//Set CSN high
}

/*
 *Function: _nrf_read_data(uint8_t reg, uint8_t *start_ptr, uint8_t length)
 *
 *Reads multiple bytes from specified register on the nRF.
 *Needed for example for reading addresses and payload data.
 */
void _nrf_read_data(uint8_t reg, uint8_t *start_ptr, uint8_t length)
{
	CSN_PORT &= ~(1<<CSN_PIN);	//Set CSN LOW
	_nrf_spi_write_byte(R_REGISTER + reg);	//Tell nRF what we will read
	for(int a=0; a<length; a++)
	{
		*start_ptr = _nrf_spi_read_byte();
		start_ptr++;
	}
	CSN_PORT |= (1<<CSN_PIN);	//Set CSN high
}

/*
 *Function: _nrf_pulse_ce()
 *
 *Outputs a 10 us high logic level pulse on the CE pin
 *Used for initiating radio TX operation
 */
void _nrf_pulse_ce()
{
	CE_PORT |= (1<<CE_PIN);
	_delay_us(10);
	CE_PORT &= ~(1<<CE_PIN);
}

/*
 *Function: nrf_start_listening()
 *
 *Puts CE pin high
 *Sets the module to PRX mode by writing to CONFIG register bit PRIM_RX. Other CONFIG register bits are kept
 *as they were
 */
void nrf_start_listening()
{
	uint8_t reg = _nrf_read_byte(CONFIG);
	reg |= (1<<PRIM_RX);
	_nrf_write_byte(CONFIG, reg);
	CE_PORT |= (1<<CE_PIN);	//set CE high
}

/*
 *Function: nrf_stop_listening()
 *
 *Puts CE pin low.
 *Sets the module to PTX mode by writing to CONFIG register bit PRIM_RX. Other CONFIG register bits are kept
 *as they were
 */
void nrf_stop_listening()
{
	CE_PORT &= ~(1<<CE_PIN);	//set CE low
	uint8_t reg = _nrf_read_byte(CONFIG);
	reg &= ~(1<<PRIM_RX);
	_nrf_write_byte(CONFIG, reg);
}

/*
 *Function: bool nrf_data_available()
 *
 *Returns true if there is RX data available
 *Checks if STATUS register bit 6: RX_DR is set (Data ready interrupt)
 */
bool nrf_data_available()
{
	if((_nrf_read_byte(STATUS) & (1<<RX_DR)) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 *Function: bool nrf_transmit_success()
 *
 *Returns true if the TX transmit was successful
 *Checks if STATUS register bit 5 is set (Data sent interrupt).
 *If auto-ack is in use, the bit is set when ack is received
 *If auto-ack is not in use, the bit is set when the transfer is complete.
 */
bool nrf_transmit_success()
{
	if((_nrf_read_byte(STATUS) & (1<<TX_DS)) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 *Function: bool nrf_transmit_fail()
 *
 *Returns true if the TX transmit failed, only applicable if auto-ack is enabled
 *Checks if the Status register MAX_RT bit is set (Maximum number of retransmissions interrupt)
 */
bool nrf_transmit_fail()
{
	if((_nrf_read_byte(STATUS) & (1<<MAX_RT)) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 *Function: nrf_write_tx_payload(uint8_t *start_ptr, uint8_t length)
 *
 *Clears TX related IRQs, flushes TX, Writes bytes to TX payload register, and then initiates transmission.
 *This is a non-blocking function, transmission finishes an unknown time after running this function.
 *Transmission status should be queried before trying to start next transmission.
 *Maximum payload length is 32 bytes.
 *The nRF module is most likely in standby 1 mode before running this command,
 *which means that there is a 130 us delay from CE to actual transmission start.
 *(Datasheet: 6.1.3.1 Standby-1 mode)
 */
void nrf_write_tx_payload(uint8_t *start_ptr, uint8_t length)
{
	_nrf_write_byte(STATUS, 0x30);	//Clear MAX_RT and TX_DS IRQ:s from STATUS register
	_nrf_read_byte(FLUSH_TX);	//Reading FLUSH_TX does the flush
	//Decrement W_REGISTER from W_TX_PAYLOAD as the bit mnemnonic should not
	//be applied to non-register-write commands (see datasheet: Data and control interface, page 51).
	_nrf_write_data(W_TX_PAYLOAD-W_REGISTER, start_ptr, length);	
	_nrf_pulse_ce();
}

/*
 *Function: nrf_read_rx_payload(uint8_t *start_ptr, uint8_t length)
 *
 *Reads the RX payload, then resets RX_DR IRQ flag and flushes RX FIFO 
 *as specified on datasheet (registry map, note c, page 63)
 *Maximum payload length is 32 bytes
 */
void nrf_read_rx_payload(uint8_t *start_ptr, uint8_t length)
{
	_nrf_read_data(R_RX_PAYLOAD, start_ptr, length);
	_nrf_write_byte(STATUS, 0x40);	//Clear RX_DR IRQ from STATUS register
	_nrf_read_byte(FLUSH_RX);	//Reading FLUSH_RX does the flush
}

/*
 *Function: nrf_set_transmit_power(uint8_t transmitPower)
 *
 *Sets the transmit power by reading RF_SETUP register,
 *then changes bits 1 & 2 (RF_PWR) (other bits remain as they were), and writes to RF_SETUP.
 *0 = -18 dBm
 *1 = -12 dBm
 *2 = -6 dBm
 *3 = 0 dBm
 */
void nrf_set_transmit_power (uint8_t transmitPower)
{
	transmitPower = (transmitPower << 1);	//has to be shifted to affect correct bits
	transmitPower &= 0b00000110;	//prevent out of bound parameter messing the register up
	uint8_t reg = _nrf_read_byte(RF_SETUP);
	reg &= (~0b00000110);	//clear bits 1 & 2
	reg |= transmitPower;	
	_nrf_write_byte(RF_SETUP, reg);
}

/*
 *Function: nrf_set_tx_address(uint8_t *address_ptr)
 *
 *Reads address width from nRF register SETUP_AW
 *then writes the TX address.
 *Address width has to be set before using this function
 */
void nrf_set_tx_address(uint8_t *address_ptr)
{
	// SETUP_AW has two first bits (0&1) meaningful, rest are always zeros
	// 0 = illegal, 01 = 3 bytes, 10 = 4 bytes, 11 = 5 bytes
	uint8_t awSetup = _nrf_read_byte(SETUP_AW);
	uint8_t length = awSetup + 0x02;	//convert register value to address length in bytes
	_nrf_write_data(TX_ADDR, address_ptr, length);
}

/*
 *Function: nrf_set_address_width(uint8_t width)
 *
 *Sets address length in the SETUP_AW register. Bits 2-7 are reserved, only zeroes allowed.
 *Accepted parameter values are 3-5 (bytes)
 */
void nrf_set_address_width(uint8_t width)
{
	width -= 0x02;
	_nrf_write_byte(SETUP_AW, width);
}

/*
 *Function: nrf_enable_auto_ack(uint8_t pipe, bool state)
 *
 *Reads the state of the EN_AA register,
 *then modifies the appropriate bit, and writes it back.
 *The nRF module has 6 data pipes, so accepted pipe numbers are 0-5.
 */
void nrf_enable_auto_ack(uint8_t pipe, bool state)
{
	uint8_t reg = _nrf_read_byte(EN_AA);
	uint8_t mask = (0x01 << pipe);
	if(state == true)
	{
		reg |= mask;
	}
	else
	{
		reg &= (~mask);
	}
	_nrf_write_byte(EN_AA, reg);
}

/*
 *Function: setDataPipeState(uint8_t pipe, bool state)
 *
 *Reads the state of the EN_RXADDR register, then 
 *modifies the appropriate bit, and writes it back.
 *The nRF module has 6 data pipes, so accepted pipe numbers are 0-5.
 */
void nrf_enable_data_pipe(uint8_t pipe, bool state)
{
		uint8_t reg = _nrf_read_byte(EN_RXADDR);
		uint8_t mask = (0x01 << pipe);
		if(state == true)
		{
			reg |= mask;
		}
		else
		{
			reg &= (~mask);
		}
		_nrf_write_byte(EN_RXADDR, reg);
}

/*
 *Function: nrf_set_rx_address(uint8_t pipe, uint8_t *address_ptr)
 *
 *Sets the address for the specified RX data pipe.
 *Writes as many bytes from the pointer as is specified in the address width register.
 *Every data pipe's address has an own register address, P0 is 0x0A and P5 is 0F
 *Address width has to be set before using this function. The nRF module has 6 data pipes (0-5).
 */
void nrf_set_rx_address(uint8_t pipe, uint8_t *address_ptr)
{
	uint8_t reg = RX_ADDR_P0;
	if(pipe < 6)	//Clamp the register value
	{
		reg += pipe;	//Set correct register address using pipe number
	}
	uint8_t length = _nrf_read_byte(SETUP_AW) + 0x02;	//convert register value to address length in bytes
	_nrf_write_data(reg, address_ptr, length);
}
/*Function: nrf_set_channel(uint8_t channel)
 *
 *Sets the RF channel of the module. First 7 bits in RF_CH register control the channel.
 *The module can use 126 channels: 0x00 is 2,400 GHz, 0x7E is 2,525GHz. 
 *This function is limited to channels 1-82 for legal reasons.
 *REMEMBER TO CHECK FREQUENCY LEGALITY!!!
 *In Finland, allowed 2,4 GHz frequency band is 2,400 GHz - 2,4835 GHz.
 *Radio bandwidth is 2 MHz @ 2Mbps, so channels are limited to ch1 (2,401 GHz) - ch82(2,482 GHz).
 */
void nrf_set_channel(uint8_t channel)
{
	//Clamp the channel to the legal frequency range
	if (channel < 1 )
	{
		channel = 1;
	}
	else if (channel > 82)
	{
		channel = 82;
	}
	_nrf_write_byte(RF_CH, channel);
}

/*
 *Function: nrf_set_data_rate(uint8_t dataRate)
 *
 *Sets the nRF modules data rate. Available selections are 250 kbps, 1 Mbps and 2 Mbps.
 *0 = 250 kbps
 *1 = 1 Mbps
 *2 = 2 Mbps
 *The data rate is controlled by the RF_SETUP register bits 3 & 5. RF_SETUP register has plenty
 *of other settings, se they are kept as is.
 *Bit 5 sets 250 kbps data rate, if set bit 3 will be don't care.
 *If bit 5 is 0, selection between 1 Mbps and 2 Mbps is done with bit 3.
 *Bit 3 true is 2 Mbps, false is 1 Mbps.
 */
void nrf_set_data_rate(uint8_t dataRate)
{
	uint8_t reg = _nrf_read_byte(RF_SETUP);
	switch (dataRate)
	{
		case 1:
			reg &= ~(0x01 << 5);	//Clear bit 5
			reg &= ~(0x01 << 3);	//Clear bit 3
			break;
		case 2:
			reg &= ~(0x01 << 5);	//Clear bit 5
			reg |= (0x01 << 3);		//Set bit 3
			break;
		default:
			reg |= (0x01 << 5);	//Set bit 5
	}
	_nrf_write_byte(RF_SETUP, reg);
}

/*
 *Function: nrf_set_payload_length(uint8_t pipe, uint8_t length)
 *
 *Sets the RX data pipe payload length.
 *Every RX data pipe has it's own address (0x11 for P0, 0x16 for P5). and payload length.
 *Payload length is selectable 0-32 bytes (0=pipe not in use)
 *Payload length is controlled by bits 0-5, bits 6-7 must be 0.
 */
void nrf_set_payload_length(uint8_t pipe, uint8_t length)
{
	uint8_t address = RX_PW_P0;
	//Limit the pipe number to max, 5
	if(pipe > 5)
	{
		pipe = 5;
	}
	address += pipe;
	//limit payload length to 32
	if (length > 32)
	{
		length = 32;
	}
	_nrf_write_byte(address, length);
}

/*
 *Function: nrf_set_retransmit_delay(uint16_t delay)
 *
 *Sets the delay between retransmissions (if enabled).
 *Parameter value is delay in microseconds.
 *The nRF module has selectable delay 250 - 4000 us. The delay settings are at 250 us intervals.
 *The delay is controlled by SETUP_RETR register bits 4-7.
 *SETUP_RETR has also other settings, so bits 0-3 have to be kept as they were.
 */
void nrf_set_retransmit_delay(uint16_t delay)
{
	//Clamp the delay to acceptable values
	if(delay < 250)
	{
		delay = 250;
	}
	else if(delay > 4000)
	{
		delay = 4000;
	}
	delay = (delay / 250) - 1;
	uint8_t delayByte = (delay & 0xFF);	//get LSB from 16bit uint
	delayByte = (delayByte<<4);	//Shift to affect bytes 4-7
	uint8_t reg = _nrf_read_byte(SETUP_RETR);
	reg &= 0x0F;	//clear bits 4-7
	reg |= delayByte;
	_nrf_write_byte(SETUP_RETR, reg);
}

/*
 *Function: nrf_set_retransmit_count(uint8_t count)
 *
 *Sets the number of retransmissions, possible values are 0-15 (0 disables retransmissions).
 *The setting is configured by bits 0-3 in the SETUP_RETR register. SETUP_RETR has also other settings,
 *so other bits have to be kept as they were.
 */
void nrf_set_retransmit_count(uint8_t count)
{
	if(count > 15)
	{
		count = 15;
	}
	uint8_t reg = _nrf_read_byte(SETUP_RETR);
	reg &= 0xF0;	//Clear bits 0-3
	reg |= count;
	_nrf_write_byte(SETUP_RETR, reg);
}

/*
 *Function: nrf_power_up()
 *
 *Sets the PWR_UP bit in the CONFIG register. Other bits in the register are kept as they were.
 *The nRF module needs at least 100 ms startup time after low voltage/brownout.
 *(Datasheet chapter 5.7 Power on reset)
 *Normal power down -> standby delay is 150 us - 4.5 ms depending on the circuit
 *(datasheet chapter 6.1.7 Timing information)
 */
void nrf_power_up()
{
	uint8_t reg = _nrf_read_byte(CONFIG);
	reg |= (1<<PWR_UP);
	_nrf_write_byte(CONFIG, reg);
}

/*
 *Function: nrf_power_down()
 *
 *Sets the PWR_UP bit in the CONFIG register to 0. Other bits are kept as they were.
 *No timing constraints are mentioned in the datahsheet for powering the module down.
 *All registry values are maintained in the power down mode, and all registers accept changes.
 *Module current consumption:
 *Power down: 900 nA
 *Standby 1: 26 uA	(Enters this when powerUp=1 and CE=low)
 *Standby 2: 320 uA	(Enters this when PTX mode, CE=high and empty TX FIFO)
 *RX/TX operation modes: 8-14 mA
 */
void nrf_power_down()
{
	uint8_t reg = _nrf_read_byte(CONFIG);
	reg &= ~(1<<PWR_UP);
	_nrf_write_byte(CONFIG, reg);
}

/*
 *Function: nrf_set_crc_length(uint8_t length)
 *
 *Sets the amount of CRC bytes in the frame. Number of bytes can be chosen between 1 and 2
 *0 disables CRC function (if any EA_AA is on, CRC is forced on)
 *CRC enable is controlled by CONFIG register bit EN_CRC (0=off, 1=on)
 *Amount of bytes is controlled by CONFIG register bit CRC0 (0=1byte, 1=2bytes)
 *CONFIG register has other settings, so other bits must be preserved as they were
 */
void nrf_set_crc_length(uint8_t length)
{
	uint8_t reg = _nrf_read_byte(CONFIG);
	if(length == 0)	//Disable CRC
	{
		reg &= ~(1<<EN_CRC);
	}
	else if(length == 1)	//Set 1 byte CRC
	{
		reg |= (1<<EN_CRC);
		reg &= ~(1<<CRCO);
	}
	else	//Set 2 bytes CRC
	{
		reg |= (1<<EN_CRC);
		reg |= (1<<CRCO);
	}
	_nrf_write_byte(CONFIG, reg);
}

/*
 *Function: nrf_enable_ack_payload(bool state)
 *
 *Sets the custom acknowledgement packet payload feature on/off by writing to FEATURE register
 *bit EN_ACK_PAY.
 *If the custom ack packet is enabled, dynamic payload length must be enabled for pipe 0 on the PTX and PRX
 *(datasheet: Register map, note d, page 63).
 *Retransmit delay must be set carefully when using ack payload (Datasheet: register map note a, page 63).
 *Retransmit delays for ack payload sizes are listed in datasheet chapter 7.4.2 Auto Retransmission
 */
void nrf_enable_ack_payload(bool state)
{
	uint8_t reg = _nrf_read_byte(FEATURE);
	if (state == true)
	{
		reg |= (1<<EN_ACK_PAY);
	}
	else
	{
		reg &= ~(1<<EN_ACK_PAY);
	}
	_nrf_write_byte(FEATURE, reg);
}

/*
 *Function: enableDynamicPayloadLength(uint8_t pipe, bool state)
 *
 *Sets the dynamic payload length for the data pipe on/off
 *Also enables/disables EN_DPL from FEATURE register as needed
 *This is done by writing to DYNPD register, bit 0 is data pipe 0 and bit 5 is data pipe 5.
 *Bits 6,7 are reserved, only 00 allowed.
 *This requires setting EN_DPL from FEATURE register, and the correct bit from EN_AA for the pipe.
 */
void nrf_enable_dynamic_payload_length(uint8_t pipe, bool state)
{
	if(pipe>5)	//Limit the pipe number to 5
	{
		pipe=5;
	}
	uint8_t dyndpReg = _nrf_read_byte(DYNPD);
	if(state == true)
	{
		dyndpReg |= (1<<pipe);	//Set dynamic payload for the set pipe
		uint8_t regFeature = _nrf_read_byte(FEATURE);
		regFeature |= (1<<EN_DPL);	//Make sure EN_DPL is set in FEATURE register
		_nrf_write_byte(FEATURE, regFeature);
	}
	else
	{
		dyndpReg &= ~(1<<pipe);	//disable dynamic payload for the set pipe
		if (dyndpReg == 0x00)	//if there are no pipes with dynamic payloads, set EN_DPL to 0 in FEATURE register
		{
			uint8_t regFeature = _nrf_read_byte(FEATURE);
			regFeature &= ~(1<<EN_DPL);
			_nrf_write_byte(FEATURE, regFeature);
		}
	}
	_nrf_write_byte(DYNPD, dyndpReg);
}

/*
 *Function: nrf_write_ack_payload(uint8_t pipe, uint8_t *data_ptr, uint8_t length)
 *
 *Writes the payload that will be sent with the next ACK packet.
 *TODO: are there flushes/flag clearings that should be done with this? Timings?
 */
void nrf_write_ack_payload(uint8_t pipe, uint8_t *data_ptr, uint8_t length)
{
	if(pipe>5)
	{
		pipe=5;
	}
	/*Writing the payload is a separate command, not a register write, so W_REGISTER must be decremented
	 *from the address (_nrf_write_data adds W_REGISTER to the address). The payload must be designated to
	 *a certain pipe. This is done by adding the pipe number to the address. */
	_nrf_write_data((W_ACK_PAYLOAD-W_REGISTER+pipe), data_ptr, length);
}
